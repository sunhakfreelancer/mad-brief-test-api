import { createRouter, HttpMethods } from "express-router-helper"
import { getAllRecords, getDetail, createNew, updateRecord, archiveRecord, applyPromoCode } from "../controllers/orders"
import checkPagination from "../middlewares/check-pagination"
import checkAuth from "../middlewares/check-auth"

export default createRouter({
  prefix: "api/orders",
  routes: [
    {
      method: HttpMethods.GET,
      path: "",
      middleware: [checkPagination, checkAuth],
      handler: getAllRecords
    },
    {
      method: HttpMethods.GET,
      path: ":id",
      middleware: [checkAuth],
      handler: getDetail
    },
    {
      method: HttpMethods.POST,
      path: "",
      middleware: [],
      handler: createNew
    },
    {
      method: HttpMethods.PUT,
      path: ":id",
      middleware: [checkAuth],
      handler: updateRecord
    },
    {
      method: HttpMethods.DELETE,
      path: ":id",
      middleware: [checkAuth],
      handler: archiveRecord
    },
    {
      method: HttpMethods.POST,
      path: ":id/apply-promo-code",
      middleware: [],
      handler: applyPromoCode
    }
  ]
})
