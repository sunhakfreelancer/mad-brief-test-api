import { createRouter, HttpMethods } from "express-router-helper"
import { getAllRecords, getDetail, createNew, updateRecord, archiveRecord, verifyPromoCode } from "../controllers/promo-codes"
import checkAuth from "../middlewares/check-auth"
import checkPagination from "../middlewares/check-pagination"

export default createRouter({
  prefix: "api/promo-codes",
  routes: [
    {
      method: HttpMethods.GET,
      path: "",
      middleware: [checkAuth, checkPagination],
      handler: getAllRecords
    },
    {
      method: HttpMethods.GET,
      path: ":id",
      middleware: [checkAuth],
      handler: getDetail
    },
    {
      method: HttpMethods.POST,
      path: "",
      middleware: [checkAuth],
      handler: createNew
    },
    {
      method: HttpMethods.PUT,
      path: ":id",
      middleware: [checkAuth],
      handler: updateRecord
    },
    {
      method: HttpMethods.DELETE,
      path: ":id",
      middleware: [checkAuth],
      handler: archiveRecord
    },
    {
      method: HttpMethods.POST,
      path: "verify",
      middleware: [],
      handler: verifyPromoCode
    }
  ]
});
