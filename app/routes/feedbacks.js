import { createRouter, HttpMethods } from "express-router-helper"
import { getAllRecords, getDetail, createNew, archiveRecord } from "../controllers/feedbacks"
import checkAuth from "../middlewares/check-auth"
import checkPagination from "../middlewares/check-pagination"

export default createRouter({
  prefix: "api/feedbacks",
  routes: [
    {
      method: HttpMethods.GET,
      path: "",
      middleware: [checkPagination, checkAuth],
      handler: getAllRecords
    },
    {
      method: HttpMethods.GET,
      path: ":id",
      middleware: [checkAuth],
      handler: getDetail
    },
    {
      method: HttpMethods.POST,
      path: "",
      middleware: [],
      handler: createNew
    },
    {
      method: HttpMethods.DELETE,
      path: ":id",
      middleware: [checkAuth],
      handler: archiveRecord
    }
  ]
})
