import { createRouter, HttpMethods } from "express-router-helper";
import { getAllRecords, getDetail, createNew, updateRecord, archiveRecord } from "../controllers/products"
import { upload } from '../middlewares/multer'
import checkAuth from "../middlewares/check-auth"
import checkPagination from "../middlewares/check-pagination"

export default createRouter({
  prefix: "api/products",
  routes: [
    {
      method: HttpMethods.GET,
      path: "",
      middleware: [checkPagination],
      handler: getAllRecords
    },
    {
      method: HttpMethods.GET,
      path: ":id",
      middleware: [],
      handler: getDetail
    },
    {
      method: HttpMethods.POST,
      path: "",
      middleware: [checkAuth, upload.single("image")],
      handler: createNew
    },
    {
      method: HttpMethods.PUT,
      path: ":id",
      middleware: [checkAuth, upload.single("image")],
      handler: updateRecord
    },
    {
      method: HttpMethods.DELETE,
      path: ":id",
      middleware: [checkAuth],
      handler: archiveRecord
    }
  ]
})
