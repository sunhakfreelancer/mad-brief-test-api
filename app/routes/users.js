import { createRouter, HttpMethods } from "express-router-helper";
import { getDetail, signUp, updateRecord, archiveRecord, login } from "../controllers/users"
import checkAuth from "../middlewares/check-auth"

export default createRouter({
  prefix: "api/users",
  routes: [
    {
      method: HttpMethods.GET,
      path: "me",
      middleware: [checkAuth],
      handler: getDetail
    },
    {
      method: HttpMethods.POST,
      path: "",
      middleware: [],
      handler: signUp
    },
    {
      method: HttpMethods.POST,
      path: "login",
      middleware: [],
      handler: login
    },
    {
      method: HttpMethods.PUT,
      path: ":id",
      middleware: [checkAuth],
      handler: updateRecord
    },
    {
      method: HttpMethods.DELETE,
      path: ":id",
      middleware: [checkAuth],
      handler: archiveRecord
    }
  ]
});
