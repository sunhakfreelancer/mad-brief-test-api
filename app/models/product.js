import mongoose, { Schema } from "mongoose"
import currentTime from "../helpers/current-time"
import uniqueValidator from "mongoose-unique-validator"
import moment from "moment-timezone"

const schema = new Schema({
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
    unique: true
  },
  price: {
    type: Number,
    required: true
  },
  image: {
    type: String
  },
  createdAt: {
    type: Date,
    default: currentTime()
  },
  updatedAt: {
    type: Date,
    default: currentTime()
  },
  archivedAt: {
    type: Date
  }
})

schema.plugin(uniqueValidator)
export default mongoose.model("Product", schema)
