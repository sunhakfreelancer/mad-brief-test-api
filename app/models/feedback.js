import mongoose, { Schema } from "mongoose"
import currentTime from "../helpers/current-time"

const schema = new Schema({
  _id: Schema.Types.ObjectId,
  phone: {
    type: String,
    required: true
  },
  text: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: currentTime()
  },
  archivedAt: {
    type: Date
  }
})

export default mongoose.model("Feedback", schema)
