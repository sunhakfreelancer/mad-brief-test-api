import mongoose, { Schema } from "mongoose"
import currentTime from "../helpers/current-time"
require("mongoose-type-email")

const STATUSES = ["PENDING", "PROCESSING", "DILIVERED"]

const schema = new Schema({
  _id: Schema.Types.ObjectId,
  quantity: {
    type: Number,
    default: 1
  },
  status: {
    type: String,
    enum: STATUSES,
    default: STATUSES[0]
  },
  customer: {
    name: {
      type: String
    },
    email: {
      type: mongoose.SchemaTypes.Email,
      allowBlank: true
    },
    address: {
      type: String
    },
    phone: {
      type: String,
      required: true
    }
  },
  product: {
    type: Schema.Types.ObjectId,
    ref: "Product",
    required: true
  },
  promoCode: {
    type: Schema.Types.ObjectId,
    ref: "PromoCode"
  },
  createdAt: {
    type: Date,
    default: currentTime()
  },
  updatedAt: {
    type: Date,
    default: currentTime()
  },
  archivedAt: {
    type: Date
  }
})

export default mongoose.model("Order", schema)
