import mongoose, { Schema } from "mongoose"
import currentTime from "../helpers/current-time"
import uniqueValidator from "mongoose-unique-validator"
require("mongoose-type-email")

const schema = new Schema({
  email: {
    type: mongoose.SchemaTypes.Email,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: currentTime()
  },
  updatedAt: {
    type: Date,
    default: currentTime()
  },
  archivedAt: {
    type: Date
  }
})

schema.plugin(uniqueValidator)
export default mongoose.model("User", schema)
