import mongoose, { Schema } from "mongoose"
import currentTime from "../helpers/current-time" 
import uniqueValidator from 'mongoose-unique-validator'

const schema = new Schema({
  _id: Schema.Types.ObjectId,
  code: {
    type: String,
    unique: true,
    required: true
  },
  value: {
    type: Number,
    min: 0,
    max: 100,
    default: 0
  },
  createdAt: {
    type: Date,
    default: currentTime()
  },
  updatedAt: {
    type: Date,
    default: currentTime()
  },
  archivedAt: {
    type: Date
  }
})

schema.plugin(uniqueValidator)
export default mongoose.model("PromoCode", schema)
