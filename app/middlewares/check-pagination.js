export default (req, res, next) => {
  let { page, limit } = req.query

  if (limit && page <= 0) page = 1
  if(!limit || !page || parseInt(page) < 0) page = null 

  limit = Math.abs(parseInt(limit))
  page = parseInt(page)

  req.query.page = page
  req.query.limit = limit
  next()
}
