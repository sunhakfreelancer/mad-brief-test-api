import jwt from "jsonwebtoken"
import formatError from "../helpers/format-error"

export default (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1]
    const decoded = jwt.verify(token, process.env.APP_SECRET_KEY)
    req.userPayload = decoded
    next()
  } catch (err) {
    res.status(401).json({ error: formatError({}, "Permission denied...")})
  }
}
