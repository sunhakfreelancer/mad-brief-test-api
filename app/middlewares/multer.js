import multer from "multer"

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads/")
  },
  filename: (req, file, cb) => {
    cb(null, new Date().toISOString() + file.originalname)
  }
})

const fileFilter = (req, file, cb) => {
  const acceptableMimeTypes = ["image/jpeg", "image/png"]
  
  acceptableMimeTypes.includes(file.mimetype) ? cb(null, true) : cb(null, false)
}

const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 5 },
  fileFilter: fileFilter
})

export { upload }