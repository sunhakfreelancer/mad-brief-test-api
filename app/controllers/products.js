import mongoose from "mongoose"
import Product from "../models/product"
import currentTime from "../helpers/current-time"

// GET--api/products
const getAllRecords = async (req, res) => {
  const { page, limit, name } = req.query

  const total = await Product.countDocuments({ name: new RegExp(name, "i"), archivedAt: null })

  const meta = {
    total,
    totalPages: Math.ceil(total / limit) || 1,
    perPage: limit,
    currentPage: page
  }

  Product.find({ name: new RegExp(name, "i"), archivedAt: null }, { __v: 0 })
    .limit(limit)
    .skip((page - 1) * limit)
    .sort({ createdAt: -1 })
    .exec()
    .then(results => {
      res.status(200).json({
        message: `Successfully fetching all products`,
        data: results,
        meta
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to fetch all products`,
        error: err
      })
    })
}

// GET --api/products/:id
const getDetail = (req, res) => {
  const { id } = req.params
  Product.findById(id)
    .exec()
    .then(result => {
      res.json({
        message: `Successfully get product detail with id: ${id}`,
        data: result
      })
    })
    .catch(err => {
      req.status(500).json({
        message: `Fail to get product detail with id: ${id}`,
        error: err
      })
    })
}

// POST --api/products
const createNew = (req, res) => {
  const { name, price } = req.body

  const product = new Product({
    _id: new mongoose.Types.ObjectId(),
    name,
    price,
    image: req.file && req.file.path,
    createdAt: currentTime(),
    updatedAt: currentTime()
  })
  product
    .save()
    .then(result => {
      res.status(201).json({
        message: `Successfully create new product`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to create new product`,
        error: err
      })
    })
}

// PUT --api/products/:id
const updateRecord = (req, res) => {
  const { id } = req.params
  const updateOps = {}

  for (const key in req.body) {
    updateOps[key] = req.body[key]
  }

  if (req.file) updateOps["image"] = req.file.path
  updateOps["updatedAt"] = currentTime()

  Product.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      res.status(200).json({
        message: `Successfully update product with id: ${id}`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to update product with id: ${id}`,
        error: err
      })
    })
}

// DELETE -- api/products/:id
const archiveRecord = (req, res) => {
  const { id } = req.params
  Product.findById(id)
    .exec()
    .then(product => {
      return product.update({
        archivedAt: currentTime(),
        name: `${product.name} (${product._id})`
      })
    })
    .then(result => {
      res.status(200).json({
        message: `Successfully delete product with id: ${id}`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to delete product with id: ${id}`,
        error: err
      })
    })
}

export { getAllRecords, getDetail, createNew, updateRecord, archiveRecord }
