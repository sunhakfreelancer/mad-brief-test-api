import mongoose from "mongoose"
import Feedback from "../models/feedback"
import currentTime from "../helpers/current-time"

// GET--api/feedbacks
const getAllRecords = async (req, res) => {
  const { page, limit, phone } = req.query

  const total = await Feedback.countDocuments({ phone: new RegExp(phone, "g"), archivedAt: null })

  const meta = {
    total,
    totalPages: Math.ceil(total / limit) || 1,
    perPage: limit,
    currentPage: page
  }

  Feedback.find({ phone: new RegExp(phone, "g"), archivedAt: null }, { __v: 0 })
    .limit(limit)
    .skip((page - 1) * limit)
    .sort({ createdAt: -1 })
    .exec()
    .then(results => {
      res.status(200).json({
        message: `Successfully fetching all feedbacks`,
        data: results,
        meta
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to fetch all feedbacks`,
        error: err
      })
    })
}

// GET --api/feedbacks/:id
const getDetail = (req, res) => {
  const { id }= req.params
  Feedback.findById(id)
    .exec()
    .then(result => {
      res.json({
        message: `Successfully get product detail with id: ${id}`,
        data: result
      })
    })
    .catch(err => {
      req.status(500).json({
        message: `Fail to get product detail with id: ${id}`,
        error: err
      })
    })
}

// POST --api/feedbacks
const createNew = (req, res) => {
  const { phone, text } = req.body

  const feedback = new Feedback({
    _id: new mongoose.Types.ObjectId(),
    phone,
    text,
    createdAt: currentTime()
  })
  feedback
    .save()
    .then(result => {
      res.status(201).json({
        message: `Successfully create new feedback`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to create new feedback`,
        error: err
      })
    })
}

// DELETE -- api/feedbacks/:id
const archiveRecord = (req, res) => {
  const { id }= req.params
  Feedback.findByIdAndUpdate(id, { archivedAt: currentTime() })
    .exec()
    .then(result => {
      res.status(200).json({
        message: `Successfully delete feedback with id: ${id}`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to delete feedback with id: ${id}`,
        error: err
      })
    })
}

export { getAllRecords, getDetail, createNew, archiveRecord }
