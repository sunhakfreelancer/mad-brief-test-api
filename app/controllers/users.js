import mongoose from "mongoose"
import User from "../models/user"
import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import currentTime from "../helpers/current-time"

// GET --api/users/me
const getDetail = (req, res) => {
  const userPayload = req.userPayload
  if (!userPayload)
    return res.status(401).json({
      message: `Authorization failed...`,
      error: {
        message: `Please make sure your token is still valid`
      }
    })

  res.status(200).json({ data: userPayload })
}

// POST --api/users
const signUp = async (req, res) => {
  const { email, password } = req.body
  const user = new User({
    email: email,
    password: password && (await bcrypt.hash(password || "", 10)) // true && 'string' => 'string'
  })

  user
    .save()
    .then(result => {
      res.status(201).json({
        message: `Signed up successfully`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Signed up failed`,
        error: err
      })
    })
}

// POST --api/users/login
const login = (req, res) => {
  const { email, password } = req.body
  const { APP_SECRET_KEY } = process.env

  User.findOne({ email: email })
    .exec()
    .then(user => {
      if (!user)
        return res.status(401).json({
          message: `Authentication failed`,
          error: {
            message: `Please input valid Email and Password`
          }
        })

      bcrypt.compare(password, user.password, (err, matched) => {
        if (!matched)
          return res.status(401).json({
            message: `Authentication failed`,
            error: {
              message: `Please input valid Email and Password`
            }
          })

        const token = jwt.sign(
          { email: user.email, id: user._id },
          APP_SECRET_KEY,
          { expiresIn: "1h" }
        )

        res.status(201).json({
          message: `Login successfully`,
          data: {
            token: token
          }
        })
      })
    })
    .catch(err => {
      res.status(401).json({
        message: `Authentication failed...`,
        error: err
      })
    })
}

// PUT --api/users/:id
const updateRecord = (req, res) => {
  const { id } = req.params
  const updateOps = {}

  for (const key in req.body) {
    if (key === "password") continue

    updateOps[key] = req.body[key]
  }

  updateOps["updatedAt"] = currentTime()

  User.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      res.status(200).json({
        message: `Successfully updated user!`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to update user`,
        error: err
      })
    })
}

// DELETE --api/users/:id
const archiveRecord = (req, res) => {
  let { id } = req.params

  User.findById(id)
    .exec()
    .then(user => {
      return user.update({
        archivedAt: currentTime(),
        email: `${user.email} (${user._id})`
      })
    })
    .then(result => {
      res.status(200).json({
        message: `Successfully deleted user`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to delete user`,
        error: err
      })
    })
}

export { getDetail, updateRecord, archiveRecord, login, signUp }
