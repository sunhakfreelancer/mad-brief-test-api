import mongoose from "mongoose"
import PromoCode from "../models/promo-code"
import currentTime from "../helpers/current-time";

// GET -- api/promo-codes
const getAllRecords = async (req, res) => {
  const { page, limit, code } = req.query

  const total = await PromoCode.countDocuments({ code: new RegExp(code, "i"), archivedAt: null })

  const meta = {
    total,
    totalPages: Math.ceil(total / limit) || 1,
    perPage: limit,
    currentPage: page
  }

  PromoCode.find({ code: new RegExp(code, "i"), archivedAt: null }, { __v: 0 })
    .limit(limit)
    .skip((page - 1) * limit)
    .sort({ createdAt: -1 })
    .exec()
    .then(results => {
      res.status(200).json({
        message: `Successfully fetching all promo codes`,
        data: results,
        meta
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to fetch all promo codes`,
        error: err
      })
    })
}

// GET -- api/promo-codes/:id
const getDetail = (req, res) => {
  const id = req.params.id
  PromoCode.findById(id)
    .exec()
    .then(result => {
      res.json({
        message: `Successfully get promo code detail with id: ${id}`,
        data: result
      })
    })
    .catch(err => {
      req.status(500).json({
        message: `Fail to get promo code detail with id: ${id}`,
        error: err
      })
    })
}

// POST -- api/promo-codes
const createNew = (req, res) => {
  const { code, value, expiration } = req.body

  const promoCode = new PromoCode({
    _id: new mongoose.Types.ObjectId(),
    code,
    value,
    createdAt: currentTime(),
    updatedAt: currentTime()
  })

  promoCode
    .save()
    .then(result => {
      res.status(201).json({
        message: `Successfully create new promo code`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to create new promo code`,
        error: err
      })
    })
}

// PUT -- api/promo-codes/:id
const updateRecord = (req, res) => {
  const { id } = req.params
  const updateOps = {}

  for (let key in req.body) {
    updateOps[key] = req.body[key]
  }

  updateOps["updatedAt"] = currentTime()

  PromoCode.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      res.status(200).json({
        message: `Successfully update promo code with id: ${id}`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to update promo code with id: ${id}`,
        error: err
      })
    })
}

// DELETE -- api/promo-codes/:id
const archiveRecord = (req, res) => {
  let id = req.params.id

  PromoCode.findById(id)
    .exec()
    .then(promoCode => {
      return promoCode.update({
        archivedAt: currentTime(),
        code: `${promoCode.code} (${promoCode._id})`
      })
    })
    .then(result => {
      res.status(200).json({
        message: `Successfully delete promo code with id: ${id}`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to delete promo code with id: ${id}`,
        error: err
      })
    })
}

// POST --api/promo-code/verify
const verifyPromoCode = (req, res) => {
  let { code } = req.body
  PromoCode.findOne({ code, archivedAt: null })
    .then(result => {
      res.status(200).json({
        message: `Successfully verified promo code: ${code}`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to verify promo code: ${code}`,
        error: {
          code: `Please make sure you enter a valid code`
        }
      })
    })
}

export { getAllRecords, getDetail, createNew, updateRecord, archiveRecord, verifyPromoCode }
