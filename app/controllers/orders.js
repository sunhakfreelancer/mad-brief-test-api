import mongoose from "mongoose"
import Order from "../models/order"
import Product from "../models/product"
import PromoCode from "../models/promo-code"
import currentTime from "../helpers/current-time"

// GET -- api/orders
const getAllRecords = async (req, res) => {
  const { page, limit, phone } = req.query

  const total = await Order.countDocuments({ "customer.phone": new RegExp(phone, "g"), archivedAt: null })

  const meta = {
    total,
    totalPages: Math.ceil(total / limit) || 1,
    perPage: limit,
    currentPage: page
  }

  Order.find({ "customer.phone": new RegExp(phone, "g"), archivedAt: null }, { __v: 0 })
    .limit(limit)
    .skip((page - 1) * limit)
    .populate("product")
    .populate("promoCode")
    .sort({ createdAt: -1 })
    .exec()
    .then(results => {
      res.status(200).json({
        message: `Successfully fetching all orders`,
        data: results,
        meta
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to fetch all orders`,
        error: err
      })
    })
}

// GET -- api/orders/:id
const getDetail = (req, res) => {
  let id = req.params.id
  Order.findById(id)
    .exec()
    .then(result => {
      res.json({
        message: `Successfully get order detail with id: ${id}`,
        data: result
      })
    })
    .catch(err => {
      req.status(500).json({
        message: `Fail to get order detail with id: ${id}`,
        error: err
      })
    })
}

// POST -- api/orders
const createNew = async (req, res) => {
  const { productId, code, quantity, customer } = req.body
  let promoCode = null

  if (code) {
    const promoCodeDoc = await PromoCode.findOne({ code, archivedAt: null })
    if (promoCodeDoc) promoCode = promoCodeDoc._id
  }

  Product.findById(productId)
    .then(result => {
      if (!result)
        return res.status(404).json({
          error: {
            message: "Product not found!"
          }
        })

      const order = new Order({
        _id: new mongoose.Types.ObjectId(),
        product: productId,
        promoCode,
        quantity,
        customer,
        createdAt: currentTime(),
        updatedAt: currentTime()
      })

      return order.save()
    })
    .then(result => {
      res.status(201).json({
        message: `Successfully create new order`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to create new order`,
        error: err
      })
    })
}

// POST -- api/orders/:id
const updateRecord = (req, res) => {
  const { id } = req.params
  const updateOps = {}
  
  for (const key in req.body) {
    updateOps[key] = req.body[key]
  }

  updateOps["updatedAt"] = currentTime()

  Order.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      res.status(200).json({
        message: `Successfully update order with id: ${id}`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to update order with id: ${id}`,
        error: err
      })
    })
}

// DELETE -- api/orders/:id
const archiveRecord = (req, res) => {
  const { id } = req.params
  Order.findByIdAndUpdate(id, { archivedAt: currentTime() })
    .exec()
    .then(result => {
      res.status(200).json({
        message: `Successfully delete order with id: ${id}`,
        data: result
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to delete order with id: ${id}`,
        error: err
      })
    })
}

// POST -- api/orders/:id/apply-promo-code
const applyPromoCode = (req, res) => {
  const { id } = req.params
  const { code } = req.body

  PromoCode.findOne({ code, archivedAt: null })
    .then(result => {
      if (!result) {
        return res.status(404).json({
          message: `Failed to apply promo code`,
          error: {
            promoCode: `Promotional Code is not valid`
          }
        })
      }

      Order.updateOne({ _id: id }, {
          promoCode: result._id,
          updatedAt: currentTime()
        }
      )
      .exec()
      .then(result => {
        res.status(201).json({
          message: `Successfully apply promo code with order id: ${id}`,
          data: result
        })
      })
    })
    .catch(err => {
      res.status(500).json({
        message: `Failed to apply promo code with order id: ${id}`,
        error: err
      })
    })
}

export { getAllRecords, getDetail, createNew, updateRecord, archiveRecord, applyPromoCode }
