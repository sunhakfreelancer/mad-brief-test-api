export default (err, message) => {
  let errors = {}
  for(let e in err) errors[e] = err[e].message

  return { message, errors }
}