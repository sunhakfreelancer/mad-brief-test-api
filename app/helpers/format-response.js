export default (meta = {}, response) => {
  return {
    meta: meta,
    data: response
  }
}
