import moment from "moment-timezone"

export default () => {
  return moment.tz("Asia/Bangkok")
}
