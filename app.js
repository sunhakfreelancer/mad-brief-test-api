require("dotenv").config()

const createError = require("http-errors")
const express = require("express")
const path = require("path")
const cookieParser = require("cookie-parser")
const logger = require("morgan")
const cors = require("cors")

const app = express()

import { productRoutes, orderRoutes, userRoutes, promoCodeRoutes, feedbackRoutes } from "./app/routes/routes"

// view engine setup
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "jade")

app.use(logger("dev"))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(cors())
app.use(express.static(path.join(__dirname, "public")))
app.use("/uploads", express.static("uploads"))

// initialize all routes
app.use(productRoutes, orderRoutes, userRoutes, promoCodeRoutes, feedbackRoutes)

app.use((req, res, next) => {
  next(createError(404, "Page not found!"))
})

app.use((err, req, res, next) => {
  res.status(err.status || 500).json({
    error: {
      message: err.message
    }
  })
})

module.exports = app
